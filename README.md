# Newborn Hugo

For a new fresh start in the world.

## Install 
For a repo with submodules, we can pull all submodules using
```
git submodule update --init --recursive
```
for the first time. All submodules will be pulled down locally.

To update submodules, we can use
```
git submodule update --recursive --remote
```
or simply
```
git pull --recurse-submodules
```
