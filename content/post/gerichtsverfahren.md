---
title: Gerichtsverfahren eingeleitet - Todesstrafe für die Covid/Impf/Masken Verbrecher
description:
author: Christian Pojoni
date: 2021-06-18T10:32:50+02:00
tags:
draft: true
---

Ein Team von über 1.000 Anwälten und über 10.000 medizinischen Experten unter der Leitung von Dr. Reiner Fuellmich hat ein Gerichtsverfahren gegen die CDC, die WHO und die Davoser Gruppe wegen Verbrechen gegen die Menschlichkeit eingeleitet. Fuellmich und sein Team präsentieren den fehlerhaften PCR-Test und die Anweisung an die Ärzte, jeden Komorbiditätstod als Covid-Tod als Betrug zu kennzeichnen. Der PCR-Test wurde nie zum Nachweis von Krankheitserregern entwickelt und ist bei 35 Zyklen zu 100% fehlerhaft. Alle von der CDC überwachten PCR-Tests sind auf 37 bis 45 Zyklen eingestellt. Die CDC räumt ein, dass Tests über 28 Zyklen für ein positives zuverlässiges Ergebnis nicht zulässig sind. Dies allein macht über 90% der angeblichen Covid-Fälle / „Infektionen“ ungültig, die durch die Verwendung dieses fehlerhaften Tests erfasst wurden.

Zusätzlich zu den fehlerhaften Tests und betrügerischen Sterbeurkunden verstößt der „experimentelle“ Impfstoff selbst gegen Artikel 32 der Genfer Konvention. Nach Artikel 32 der Genfer Konvention IV von 1949 sind „Verstümmelungen und medizinische oder wissenschaftliche Experimente, die für die medizinische Behandlung einer geschützten Person nicht erforderlich sind“, verboten. Gemäß Artikel 147 ist die Durchführung biologischer Experimente an geschützten Personen ein schwerwiegender Verstoß gegen das Übereinkommen.

Der „experimentelle“ Impfstoff verstößt gegen alle 10 Nürnberger Kodizes, die die Todesstrafe für diejenigen vorsehen, die gegen diese internationalen Gesetze verstoßen wollen.

Der „Impfstoff“ erfüllt nicht die folgenden fünf Anforderungen, um als Impfstoff zu gelten, und ist per Definition ein medizinisches „Experiment“ und eine Studie:

1. Bietet Immunität gegen das Virus
Dies ist eine „undichte“ Gentherapie, die keine Immunität gegen Covid bietet. 60% der Patienten, die eine ER oder Intensivstation mit Covid-Infektionen benötigen, behaupten, die Symptome zu reduzieren, obwohl sie doppelt geimpft wurden.

2. Schützt Empfänger vor dem Virus
Diese Gentherapie bietet keine Immunität und doppelt geimpfte können das Virus immer noch fangen und verbreiten.

3. Reduziert Todesfälle durch die Virusinfektion
Diese Gentherapie reduziert nicht die Todesfälle durch die Infektion. Mit Covid infizierte Doppelimpfungen sind ebenfalls gestorben.

4. Reduziert die Durchblutung des Virus
Diese Gentherapie erlaubt immer noch die Ausbreitung des Virus, da sie keine Immunität gegen das Virus bietet.

5. Reduziert die Übertragung des Virus
Diese Gentherapie ermöglicht weiterhin die Übertragung des Virus, da sie keine Immunität gegen das Virus bietet.



Folgende Verstöße gegen den Nürnberger Kodex sind:

## Nürnberger Kodex Nr. 1: Freiwillige Zustimmung ist unabdingbar

Niemand sollte gezwungen werden, ohne Einverständniserklärung ein medizinisches Experiment durchzuführen. Viele Medien, politische und nicht medizinische Personen fordern die Menschen auf, die Aufnahme zu machen. Sie bieten keine Informationen über die Nebenwirkungen oder Gefahren dieser Gentherapie. Alles, was Sie von ihnen hören, ist - "sicher und effektiv" und "Vorteile überwiegen die Risiken". Länder setzen Sperrungen, Zwang und Drohungen ein, um Menschen zu zwingen, diesen Impfstoff zu nehmen, oder um unter dem Mandat eines Impfpasses oder eines Green Pass die Teilnahme an der freien Gesellschaft zu verbieten. Während der Nürnberger Prozesse wurden sogar die Medien strafrechtlich verfolgt und Mitglieder getötet, weil sie die Öffentlichkeit belogen hatten, zusammen mit vielen Ärzten und Nazis, die wegen Verbrechen gegen die Menschlichkeit für schuldig befunden wurden.

## Nürnberger Code Nr. 2: Fruchtbare Ergebnisse erzielen, die mit anderen Mitteln nicht heilbar sind

Wie oben aufgeführt, erfüllt die Gentherapie nicht die Kriterien eines Impfstoffs und bietet keine Immunität gegen das Virus. Es gibt andere medizinische Behandlungen, die fruchtbare Ergebnisse gegen Covid liefern, wie Ivermectin, Vitamin D, Vitamin C, Zink und ein gestärktes Immunsystem gegen Grippe und Erkältungen.

## Nürnberger Code Nr. 3: Basisexperimente auf Ergebnissen von Tierversuchen und Naturgeschichte von Krankheiten

Diese Gentherapie übersprang Tierversuche und ging direkt zu Versuchen am Menschen über. In der von Pfizer verwendeten mRNA-Forschung - einer Kandidatenstudie zu mRNA mit Rhesusaffenaffen unter Verwendung von BNT162b2-mRNA und in dieser Studie entwickelten alle Affen eine Lungenentzündung, aber die Forscher betrachteten das Risiko als gering, da es sich um junge gesunde Affen im Alter von 2 bis 4 Jahren handelte. Israel hat Pfizer verwendet und der Internationale Gerichtshof hat eine Klage für 80% der Empfänger mit Lungenentzündung aufgrund der Injektion dieser Gentherapie akzeptiert. Trotz dieser alarmierenden Entwicklung entwickelte Pfizer seine mRNA für Covid ohne Tierversuche.

## Nürnberger Code Nr. 4: Vermeiden Sie unnötige Leiden und Verletzungen

Seit der Einführung des Experiments, das unter dem CDC VAERS-Meldesystem aufgeführt ist, wurden in Amerika über 4.000 Todesfälle und 50.000 Impfverletzungen gemeldet. In der EU wurden über 7.000 Todesfälle und 365.000 Impfverletzungen gemeldet. Dies ist eine schwerwiegende Verletzung dieses Codes.

## Nürnberger Code Nr. 5: Kein Experiment durchzuführen, wenn Grund zu der Annahme besteht, dass Verletzungen oder der Tod eintreten werden

Siehe Nr. 4, basierend auf faktenbasierten medizinischen Daten, verursacht diese Gentherapie Tod und Verletzung. Frühere Forschungen zu mRNA zeigen auch mehrere Risiken, die für dieses aktuelle Studiengenexperiment ignoriert wurden. Eine Studie von 2002 über SARS-CoV-1-Spike-Proteine ​​zeigte, dass sie Entzündungen, Immunopathologie, Blutgerinnsel verursachen und die Angiotensin 2-Expression behindern. Dieses Experiment zwingt den Körper, dieses Spike-Protein zu produzieren, das all diese Risiken erbt.

## Nürnberger Code Nr. 6: Das Risiko sollte den Nutzen niemals überschreiten

Covid-19 hat eine Wiederfindungsrate von 98-99%. Die Impfverletzungen, Todesfälle und Nebenwirkungen der mRNA-Gentherapie übertreffen dieses Risiko bei weitem. Die Verwendung von „undichten“ Impfstoffen für die Landwirtschaft wurde von den USA und der EU aufgrund der Marek Chicken-Studie verboten, die zeigt, dass „heiße Viren“ und Varianten auftreten, was die Krankheit noch tödlicher macht. Dies wurde jedoch von der CDC für den menschlichen Gebrauch ignoriert, da sie das Risiko neuer tödlicherer Varianten durch undichte Impfungen vollständig kannte. Die CDC ist sich voll und ganz bewusst, dass die Verwendung von undichten Impfstoffen die Entstehung heißer (tödlicherer) Stämme erleichtert. Dies haben sie jedoch ignoriert, wenn es um Menschen geht

## Nürnberger Code Nr. 7: Es müssen Vorbereitungen getroffen werden, um Verletzungen, Behinderungen oder den Tod aus der Ferne zu verhindern

Es wurden keine Vorbereitungen getroffen. Diese Gentherapie übersprang Tierversuche. Die klinischen Phase-3-Studien der Pharmaunternehmen am Menschen werden erst 2022/2023 abgeschlossen. Diese Impfstoffe wurden im Rahmen eines Nur-Notfall-Gesetzes zugelassen und einer falsch informierten Öffentlichkeit aufgezwungen. Sie sind NICHT von der FDA zugelassen.

## Nürnberger Code Nr. 8: Das Experiment muss von wissenschaftlich qualifizierten Personen durchgeführt werden

Politiker, Medien und Akteure, die behaupten, dies sei ein sicherer und wirksamer Impfstoff, sind nicht qualifiziert. Propaganda ist keine medizinische Wissenschaft. Viele Einzelhandelsgeschäfte wie Walmart & Drive-Through-Impfstoffzentren sind nicht qualifiziert, experimentelle medizinische Gentherapien an die nicht informierte Öffentlichkeit zu verabreichen.

## Nürnberger Code Nr. 9: Jeder muss die Freiheit haben, das Experiment jederzeit zu beenden

Trotz des Aufschreis von über 85.000 Ärzten, Krankenschwestern, Virologen und Epidemiologen wird das Experiment nicht beendet. Tatsächlich gibt es derzeit viele Versuche, Gesetze zu ändern, um die Einhaltung von Impfstoffen zu erzwingen. Dies schließt obligatorische und Zwangsimpfungen ein. Experimentelle „Update“ -Aufnahmen sind alle 6 Monate geplant, ohne auf die wachsende Anzahl von Todesfällen und Verletzungen zurückzugreifen, die bereits durch dieses Experiment verursacht wurden. Diese "Update" -Aufnahmen werden ohne klinische Studien durchgeführt. Hoffentlich wird dieser neue Nürnberger Prozess diesem Verbrechen gegen die Menschlichkeit ein Ende setzen.

## Nürnberger Code Nr. 10: Der Wissenschaftler muss das Experiment jederzeit beenden, wenn eine wahrscheinliche Ursache für eine Verletzung oder den Tod vorliegt

Aus den statistischen Berichtsdaten geht hervor, dass dieses Experiment zu Tod und Verletzung führt, aber alle Politiker, Pharmaunternehmen und sogenannten Experten versuchen nicht, dieses Gentherapie-Experiment daran zu hindern, einer falsch informierten Öffentlichkeit Schaden zuzufügen.

Was können Sie tun, um diesem Verbrechen gegen die Menschlichkeit ein Ende zu setzen? Teilen Sie diese Informationen. Machen Sie Ihre Politiker, Medien, Ärzte und Krankenschwestern zur Rechenschaft - wenn sie an diesem Verbrechen gegen die Menschlichkeit beteiligt sind, unterliegen auch sie den Gesetzen der Genfer Konvention und des Nürnberger Kodex und können vor Gericht gestellt, für schuldig befunden und getötet werden. Gerichtsverfahren schreiten voran, Beweise wurden gesammelt und eine große Zahl von Experten schlägt Alarm.

Besuchen Sie die Website des Covid Committee unter: [Link zu corona-ausschuss.de (sicher)]. Wenn Sie von diesem Verbrechen betroffen sind, melden Sie das Ereignis, die beteiligten Personen und so viele Details auf der folgenden Website:

[Link zu www.securewhistleblower.com (sicher)]

Verbrechen gegen die Menschlichkeit betreffen uns alle. Sie sind ein Verbrechen gegen Sie, Ihre Kinder, Ihre Eltern, Ihre Großeltern, Ihre Gemeinde, Ihr Land und Ihre Zukunft. https://www.medicdebate.org/de/node/2053