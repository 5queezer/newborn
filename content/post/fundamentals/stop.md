---
title: 6. Stop Loss
description:
author: Christian Pojoni
date: 2021-05-14T03:46:19+02:00
tags:
draft: true
---

So we know we must have a stop loss to determine our Position Size, as [#step-1]({{< ref "risk.md" >}}) suggests.

However, we need to know what our plan is for our trade with that stop loss once the trade moves in our direction.

Examples: 

 - Trailing Stop Loss
  -- This follows price up or down, based on the direction of your trade, by the determined amount you input.
  -- So if the coin is $100. And your trailing stop loss is $20. For every $1 the price goes up, so let's say it climbed to $110. That means the trailing stop will automatically adjust your stop loss to $90. 

 - Market Stop
  -- This is recommended for stop losses. It gets you out of the trade when it goes against you. Be sure to have "Reduce Only" ticked also. 

Before you take an entry, you are going to want to define your rules for manipulating your stop loss once the trade is in action. Or you will become your own worst enemy during the trade.

