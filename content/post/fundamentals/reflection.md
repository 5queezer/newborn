---
title: 9. Reflection
description:
author: Christian Pojoni
date: 2021-05-14T03:52:38+02:00
tags:
draft: true
---

Each trade should be a slow, well respected process. Not treated like a pokie machine where you're hanging to press "spin" again. Make sure you're taking your time with this, the market isn't going anywhere. There is always another trade. If you feel like you're in a hurry to get in, or get out of a trade, it could be emotions playing over logic. The fear of missing out, and fear, uncertainty and doubt can be very strong on some people. So, be sure to take the time to audit yourself.

You want to get your entire process to a point a professional could open a ring binder folder, with your entire trade plan, strategy and rules and understand exactly what your method of approach is.

[#personalities](https://www.16personalities.com/)  - is a great tool to find out inner quirks about yourself also. Are you someone who tries to recreate the wheel all the time like me? If so, a defined trading approach may not be your thing at times, because you always feel there is room for improvement. However, if you're a logical person, and know the defined trading approach has great results, then you'll be more inclined to use it. 

Trading is Psychology more so than "Intellect". If you can conquer yourself, you'll dominate in trading.
﻿