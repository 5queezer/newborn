---
title: 3. Trading Strategy Type
description:
author: Christian Pojoni
date: 2021-05-14T03:17:33+02:00
tags:
draft: true
---

There are many different forms of Trading Strategy types, and to simplify it, we'll break it down into categories based on Time.

 - Scalping (Low time frame anything under 15m candles)
 - Intra-day (15m candles to 4hr candles)
 - Swing (4hr candles to 1d candles)
 - Investing (weekly and above)

By determining the type of trade prior to taking it, we know what our rules will be based around taking profit.

Example:

A scalper is only looking to make a quick gain, in and out within minutes, to hours. They're not usually looking to get the exact bottom, or the exact top, but constantly and consistently taking a chunk of the middle. They also understand that there will be more losses with this aggressive style of trading, but also know as long as their R:R is positive, at the end of the day, they'll be ahead. Losses are a serious part of a scalpers game. 

Intra-day traders will look to find trades that will play out in less than a day ideally. Finding a key set up prior to a break out is the most common, and then riding that before reaching it's short term targets. 

Swing traders are looking for that perfect entry, to ride the trend until the trend reverses, usually using a defined method approach to their entry and exit.

Investors just want to buy low, and sell high. Understanding weekly time frame technical analysis helps here.


So once we've got our duration decided on, we'll then look to find an entry on one of those trade set ups.
