---
title: 7. Execute
description:
author: Christian Pojoni
date: 2021-05-14T03:51:29+02:00
tags:
draft: true
---

So you've defined your entry, your targets and your stop loss, and the rules of manipulation of the trade, prior to taking the trade.

Now we're ready to execute.

Execute the trade. And then stick to your rule based strategy once it's in motion. Do not go outside of your rules. Most traders are their own worst enemy when it comes to this. Your data should back your reason to take a trade. So let the data play true. Don't manipulate your trades unless your rules allow you to do so.

If it's a swing trade, be sure to not over analyse your trade, and focus on simple things such as higher highs and higher lows on the 4 hour time frame. You don't need to be staring at your chart all day every day for these types of trades. If you find you are staring at them, and all you're doing is swing trading, you're either bored, or over invested. Be sure to not let either effect your trade.

