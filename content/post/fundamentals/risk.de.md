---
title: Riskomanagement
description:
author: Christian Pojoni
date: 2021-05-14T00:37:29+02:00
tags:
draft: true
katex: true
---

Zunächst müssen wir sicherstellen, dass wir das Risikomanagement verstehen, da dies der wichtigste Teil des Handels ist. 

Um unser Risiko zu managen, müssen wir definieren, wie viel wir bereit sind zu verlieren, BEVOR wir den Handel eingehen.

Die Formel, die wir hier verwenden, ist die folgende:


$$\frac{\text{Eigenkapital gesamt} \cdot \text{Risiko \\%}}{\text{Abstand zum Stop Loss zum Einstieg}}$$ 


Dies sieht wie folgt aus:

Das Handelskonto hat einen Wert von $50.000 USD und unsere Risikobereitschaft liegt bei durchschnittlich 2%, während unser Stop-Loss 5% von unserem Einstieg entfernt ist. In einer mathematischen Formel sieht das so aus:

$$\frac{50000 \cdot 0.02}{0.05} = 20000$$

Wir dürfen also $20.000 unseres Kontos verwenden, solange unser Stop-Loss an der Stelle liegt, an der wir ihn vor der Aufnahme des Handels gesetzt haben. Das heißt, wenn diese Position gestoppt wird, verlieren wir nur 2% unseres gesamten Handelskontos. 

Das sieht im Diagramm so aus.

https://www.tradingview.com/x/Bw7XHv8a/

![](https://www.tradingview.com/x/Bw7XHv8a/)

Wenn wir diese Formel verwenden, bleiben wir im Spiel, solange wir eine bewährte und getestete Handelsstrategie verwenden.
