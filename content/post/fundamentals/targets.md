---
title: 5. Targets
description:
author: Christian Pojoni
date: 2021-05-14T03:43:00+02:00
tags:
draft: true
---

We need to know where we're taking profit, we need to have a plan. Or we're going to continue to extend the goal post and fail to take profit, and then have the trade turn against us and be stuck like a deer in front of a cars headlights with the inability to take action.

So, how can we find targets? Or, know when to take profit?

Examples of targets:

 - Measured Moves (The distance that a fractal / pattern suggests price should reach)
 - Fibonacci Retracement Levels
 - Key Levels
 - Order Blocks
 - Invalidation of trend.

## Example of Measured Moves
Bull Flag T1 is the distance from the first impulse down or the widest point within the flag.

![](https://www.tradingview.com/x/0hHnSkWP/)


Bull flag T2 is the distance of the impulse move up, from the point of break (or retest)

## Example of Fibonacci Retracement Targets

During an uptrend, the 0.618% fib holds, reclaims the 0.382% fib, so we target the -0.618% retracement level as our take profit level.

![](https://www.tradingview.com/x/kBaNVNMk/)

## Example of Trend Invalidation

Once price closes under our trend line, we close our LONG position, before waiting to either short because trend is changing, or, wait for another clear and clean bullish set up before re entering.

![](https://www.tradingview.com/x/7xII1uiQ/)
