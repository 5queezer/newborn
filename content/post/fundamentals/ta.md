---
title: 2. Technical Analysis
description:
author: Christian Pojoni
date: 2021-05-14T03:15:56+02:00
tags:
draft: true
---

we need to delve through the higher time frames, down to the lower. So before we even take a trade, we want to analyse the chart and make sure we're identifying any key levels of interest that may play a role in our trade. Either as a support to long off, or a resistance to short from for example.

We start on the weekly time frame, and find these key levels. 
 - Horizontals
 - Order Blocks / Demand and Supply Zones
 - Fibonacci Replacement Levels
 - Indicators
 - Identifying Trend

We then move down to the daily time frame and repeat the same process, and then the 4 hour time frame.

By doing this we're mapping out the path of least resistance for our trade.

If the trend is up on all time frames, and we've reclaimed a Demand / Supply zone into support, then we'll be looking to use a Long strategy off it.

Example: Price has tested and proven the demand zone to be support. It's then broken trend resistance. And has higher lows. It then retests demand after breaking trend resistance. And gives us a valid entry for a long, which is when we then decide the type of trade we'll be taking from this point.

https://www.tradingview.com/x/8RwX7SQ7/

![](https://www.tradingview.com/x/8RwX7SQ7/)