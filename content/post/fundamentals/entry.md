---
title: 4. Entry
description:
author: Christian Pojoni
date: 2021-05-14T03:18:27+02:00
tags:
draft: true
---

So a defined strategy will have a list based approach to giving you the right to take an entry.

There are also fluid type strategies that just take parts of price action, indicators etc, and give you a point type system to be allowed to take an entry.

Examples:

 - Defined Strategy 
  -- Ichimoku - Specific set of variables need to be met before being allowed to take an entry.
  --- Example: Price Above Green Cloud. Price above Base and Conversion Line. Lagging line above Price. Kumo trending up. Valid entry for a long.
![](https://www.tradingview.com/x/WS4vlbdW/)

- Fluid Strategy
  -- Pinbar + Key level + RSI bull div + 0.382% reclaim = 4 points, enough for us to be allowed to take an entry.
  --- Example: 4hr Bullish RSI Divergence. 0.618% held. Key level held. Trend Breached.
![](https://www.tradingview.com/x/Ze1cBLX0/)

Mastering your entries can allow you to have greater position sizes without risking more than your defined Risk Appetite, however, having your stop loss too close, too often, in a volatile market can usually lead to being stopped out more often than not. Don't be too greedy.
