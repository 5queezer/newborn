---
title: 8. Journal
description:
author: Christian Pojoni
date: 2021-05-14T03:52:10+02:00
tags:
draft: true
---

Journals are one of the best forms of data gathering as a trader. So the more information you can record, the more you'll be able to refine yourself in the months or hopefully years of your trading career.

You can record information PRIOR to taking the trade. You can record information DURING your trade and you can record information AFTER your trade.

The more you gather, the more you're going to find your own demons, which will result in hopefully being able to squash them. It will also define your edge.

Examples of things to record:

 - Day
 - Time
 - Time Frame
 - Emotions (before/during/after)
 - Exchange used
 - Ticker Traded
 - Strategy Used
