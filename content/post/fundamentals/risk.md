---
title: 1. Risk Management
description:
author: Christian Pojoni
date: 2021-05-14T00:37:29+02:00
tags:
draft: true
katex: true
---

First, we need to make sure we understand Risk Management, as this is the most important part of trading. 

To manage our risk, we need to define how much we're willing to lose BEFORE taking the trade.

The formula we use here is the following:


$$\frac{\text{Total Equity} \cdot \text{Risk \\%}}{\text{Distance to stop loss from entry}}$$ 


This looks like this:

Trading Account is worth $50,000 USD and our Risk Appetite Average is 2%, while our stop loss is 5% away from our entry. In a mathematical formula, it looks like this:

$$\frac{50000 \cdot 0.02}{0.05} = 20000$$

So we’re allowed to use $20,000 of our account, as long as our stop loss is where we state we're putting it prior to taking the trade. That means, when this position gets stopped, we only lose 2% of our entire trading account. 

Which looks like this on the chart.

https://www.tradingview.com/x/Bw7XHv8a/

![](https://www.tradingview.com/x/Bw7XHv8a/)

using this formula, will keep you in the game, as long as you're using a proven and tested trading strategy.
